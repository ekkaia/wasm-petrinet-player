# Petrinet player

Application to create Petri nets made in Rust with WebAssembly and Vue 3 TypeScript. Available online at https://ekkaia.gitlab.io/wasm-petrinet-player/. The Petri net abstract syntax has been generated from an Ecore metamodel with the Ecore2Rust tool. See more here: https://gitlab.univ-nantes.fr/E187954Y/ecore2rust.

## Usage

The latest versions of Node and Rust must be installed.

1. `npm ci`

2. `npm run dev`

3. Local server runs on `localhost:3000`

## Features supported

- Creating a Petri net with a graphical interface.
- Dark mode/light mode
- Move Petri net nodes
- Fire a transition

## Features planned

- Fire multiple transitions at once
- Remove a transition, a place or an edge
- Export to PNG
- Save Petri net to file and load a Petri net from file
