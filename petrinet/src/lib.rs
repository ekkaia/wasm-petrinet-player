mod petrinet;
mod petrinet_proxy;

use std::{cell::RefCell, rc::Rc};
use text_io::scan;
use colored::*;

use crate::{petrinet::{Petrinet, Place, Transition, Orientation}, petrinet_proxy::PetrinetProxy};

pub fn test() {
    let mut petrinet: Petrinet = Petrinet::new();

    let place_index_1: Rc<RefCell<Place>> = petrinet.add_place(String::from("P1"), 4);
    let place_index_2: Rc<RefCell<Place>> = petrinet.add_place(String::from("P2"), 0);
    let place_index_3: Rc<RefCell<Place>> = petrinet.add_place(String::from("P3"), 0);

    let transition_index_1: Rc<RefCell<Transition>> = petrinet.add_transition(String::from("T1"));
    let transition_index_2: Rc<RefCell<Transition>> = petrinet.add_transition(String::from("T2"));

    // P1 -> T1
    petrinet.add_edge(&place_index_1, &transition_index_1, Orientation::Transition, 1);
    // P2 -> T2
    petrinet.add_edge(&place_index_2, &transition_index_2, Orientation::Transition, 1);
    // P3 -> T2
    petrinet.add_edge(&place_index_3, &transition_index_2, Orientation::Transition, 1);
    // T1 -> P2
    petrinet.add_edge(&place_index_2, &transition_index_1, Orientation::Place, 1);
    // T1 -> P3
    petrinet.add_edge(&place_index_3, &transition_index_1, Orientation::Place, 1);
    // T2 -> P2
    petrinet.add_edge(&place_index_2, &transition_index_2, Orientation::Place, 1);

    println!("{:?}", petrinet.display());

    println!("{fire_result}", fire_result = petrinet.fire(&transition_index_2));

    println!("{:?}", petrinet.display());
}

pub fn cli() {
    println!("{}", "Welcome to Petrinet CLI.".bold().blue());
    let petrinet = PetrinetProxy::new();
    commands(petrinet);
}

pub fn commands(mut petrinet: PetrinetProxy) {
    println!("{}", "\nUse the following commands to build your Petri net:".bold().blue());
    println!("{}", "  [1] add a place\t[2] add a transition\t[3] add an edge".blue());
    println!("{}", "  [4] fire a transition\t[5] display petrinet\t[6] exit".blue());
    let command: usize;
    scan!("{}", command);
    match command {
        1 => {
            let name: String;
            let tokens: i32;
            println!("{}", "\nEnter information about the place: <name> <tokens>".blue());
            scan!("{} {}", name, tokens);
            let msg = format!("{:?}", petrinet.add_place(name, tokens));
            println!("{}", msg.magenta().italic());
            commands(petrinet);
        }
        2 => {
            let name: String;
            println!("{}", "\nEnter information about the transition: <name>".blue());
            scan!("{}", name);
            let msg = format!("{:?}", petrinet.add_transition(name));
            println!("{}", msg.magenta().italic());
            commands(petrinet);
        }
        3 => {
            let place_name: String;
            let transition_name: String;
            let orientation: bool;
            let weight: i32;
            println!("{}", "\nEnter information about the edge: <place_name> <transition_name> <orientation> <weight>".blue());
            println!("{}", "  Orientation is a boolean: 'true' for an edge pointing to a place and 'false' for an edge pointing to a transition.".blue());
            scan!("{} {} {} {}", place_name, transition_name, orientation, weight);
            let msg = format!("{:?}", petrinet.add_edge(place_name, transition_name, orientation, weight));
            println!("{}", msg.magenta().italic());
            commands(petrinet);
        }
        4 => {
            let transition_name: String;
            println!("{}", "\nEnter information about the transition to fire: <name>".blue());
            scan!("{}", transition_name);
            let msg = format!("{:?}", petrinet.fire(transition_name));
            println!("{}", msg.magenta().italic());
            commands(petrinet);
        }
        5 => {
            let msg = format!("{:?}", petrinet.display());
            println!("{}", msg.magenta().italic());
            commands(petrinet);
        }
        6 => {
            println!("{}", "\nExit...".italic());
        }
        _ => {
            println!("{}", "\ninvalid command!".italic());
        }
    }
}