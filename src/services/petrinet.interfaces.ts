export interface Place {
	name: string;
	tokens: number;
}

export interface Transition {
	name: string;
}

export interface Edge {
	place: Place;
	transition: Transition;
	orientation: boolean;
	weight: number;
}

export interface DisplayEdges {
	[key: string]: DisplayEdge;
}

export interface DisplayNodes {
	[key: string]: DisplayNode;
}

export interface DisplayEdge {
	weight: number;
	source: string;
	target: string;
}

export interface DisplayNode {
	name: string;
	type: string;
	tokens?: number;
}

export interface Report {
	text: string;
	isError: boolean;
}

export type PetrinetMessage =
	| "FireFailed"
	| "FireImpossible"
	| "FireSuccessful"
	| "NameAlreadyUsed"
	| "EdgeAdded"
	| "EdgeAlreadyExist"
	| "TransitionAdded"
	| "TransitionDoesNotExist"
	| "PlaceAdded"
	| "PlaceDoesNotExist";

export const messages: Record<PetrinetMessage, Report> = {
	FireFailed: {
		text: "Fire failed",
		isError: true,
	},
	FireImpossible: {
		text: "Fire impossible",
		isError: true,
	},
	FireSuccessful: {
		text: "Fire successful",
		isError: false,
	},
	NameAlreadyUsed: {
		text: "Name already used",
		isError: true,
	},
	EdgeAdded: {
		text: "Edge added",
		isError: false,
	},
	EdgeAlreadyExist: {
		text: "Edge already exist",
		isError: true,
	},
	TransitionAdded: {
		text: "Transition added",
		isError: false,
	},
	TransitionDoesNotExist: {
		text: "Transition does not exist",
		isError: true,
	},
	PlaceAdded: {
		text: "Place added",
		isError: false,
	},
	PlaceDoesNotExist: {
		text: "Place does not exist",
		isError: true,
	},
};
