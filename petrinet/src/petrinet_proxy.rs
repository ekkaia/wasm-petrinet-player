use serde::{Serialize, Deserialize};
use wasm_bindgen::prelude::*;
use std::{cell::RefCell, rc::Rc};
use crate::petrinet::{Petrinet, Place, Edge, Transition, Orientation};

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Message {
    FireFailed = "FireFailed",
    FireImpossible = "FireImpossible",
    FireSuccessful = "FireSuccessful",
    NameAlreadyUsed = "NameAlreadyUsed",
    EdgeAdded = "EdgeAdded",
    EdgeAlreadyExist = "EdgeAlreadyExist",
    TransitionAdded = "TransitionAdded",
    TransitionDoesNotExist = "TransitionDoesNotExist",
    PlaceAdded = "PlaceAdded",
    PlaceDoesNotExist = "PlaceDoesNotExist",
}

#[wasm_bindgen]
pub struct PetrinetProxy {
    petrinet: Petrinet,
}

#[wasm_bindgen]
impl PetrinetProxy {
    #[wasm_bindgen(constructor)]
    pub fn new() -> PetrinetProxy {
        PetrinetProxy {
            petrinet: Petrinet {
                places: Vec::new(),
                transitions: Vec::new(),
                edges: Vec::new(),
            }
        }
    }

    pub fn add_place(&mut self, name: String, tokens: i32) -> Message {
        let is_name_already_used: Option<Rc<RefCell<Place>>> = self.find_place_by_name(name.clone());
        match is_name_already_used {
            None => {
                self.petrinet.add_place(name, tokens);
                return Message::PlaceAdded;
            },
            Some(_) => Message::NameAlreadyUsed,
        }
    }

    pub fn add_transition(&mut self, name: String) -> Message {
        let is_name_already_used: Option<Rc<RefCell<Transition>>> = self.find_transition_by_name(name.clone());
        match is_name_already_used {
            None => {
                self.petrinet.add_transition(name);
                return Message::TransitionAdded;
            },
            Some(_) => Message::NameAlreadyUsed,
        }
    }

    pub fn add_edge(&mut self, place_name: String, transition_name: String, orientation: bool, weight: i32) -> Message {
        let opt_rc_place: Option<Rc<RefCell<Place>>> = self.find_place_by_name(place_name);
        return match opt_rc_place {
            Some(rc_place) => {
                let opt_rc_transition: Option<Rc<RefCell<Transition>>> = self.find_transition_by_name(transition_name);
                match opt_rc_transition {
                    Some(rc_transition) => {
                        let orientation_enum: Orientation = if orientation { Orientation::Place } else { Orientation::Transition };
                        let is_name_already_used: Option<Rc<RefCell<Edge>>> = self.find_edge_by_input_output(Rc::clone(&rc_transition), Rc::clone(&rc_place), orientation_enum);
                        match is_name_already_used {
                            Some(_) => Message::EdgeAlreadyExist,
                            None => {
                                self.petrinet.add_edge(&rc_place, &rc_transition, orientation_enum, weight);
                                return Message::EdgeAdded;
                            }
                        }
                    },
                    None => Message::TransitionDoesNotExist,
                }
            },
            None => Message::PlaceDoesNotExist,
        };
    }

    fn find_place_by_name(&mut self, place_name: String) -> Option<Rc<RefCell<Place>>> {
        let mut rc_place: Option<Rc<RefCell<Place>>> = None;
        for place in self.petrinet.places.iter() {
            if place_name == place.borrow().name {
                rc_place = Some(Rc::clone(&place));
            }
        }
        return rc_place;
    }

    fn find_transition_by_name(&mut self, transition_name: String) -> Option<Rc<RefCell<Transition>>> {
        let mut rc_transition: Option<Rc<RefCell<Transition>>> = None;
        for transition in self.petrinet.transitions.iter() {
            if transition_name == transition.borrow().name {
                rc_transition = Some(Rc::clone(&transition));
            }
        }
        return rc_transition;
    }

    fn find_edge_by_input_output(&mut self, transition: Rc<RefCell<Transition>>, place: Rc<RefCell<Place>>, orientation: Orientation) -> Option<Rc<RefCell<Edge>>> {
        let mut rc_edge: Option<Rc<RefCell<Edge>>> = None;
        for edge in self.petrinet.edges.iter() {
            if edge.borrow().transition.borrow().name == transition.borrow().name
            && edge.borrow().place.borrow().name == place.borrow().name
            && edge.borrow().orientation == orientation {
                rc_edge = Some(Rc::clone(&edge));
            }
        }
        return rc_edge;
    }

    pub fn fire(&mut self, transition_name: String) -> Message {
        let rc_transition: Option<Rc<RefCell<Transition>>> = self.find_transition_by_name(transition_name);
        match rc_transition {
            Some(rc) => {
                let is_fire_possible = self.petrinet.fire(&rc);
                if is_fire_possible {
                    return Message::FireSuccessful;
                } else {
                    return Message::FireImpossible;
                }
            },
            None => Message::FireFailed,
        }
    }

    #[wasm_bindgen(method, getter)]
    pub fn places(&self) -> JsValue {
        let mut places: Vec<LightPlace> = Vec::new();
        for place in self.petrinet.places.iter() {
            places.push(LightPlace {
                name: place.borrow().name.clone(),
                tokens: place.borrow().tokens,
            });
        }
        return JsValue::from_serde(&places).unwrap();
    }

    #[wasm_bindgen(method, getter)]
    pub fn transitions(&self) -> JsValue {
        let mut transitions: Vec<LightTransition> = Vec::new();
        for transition in self.petrinet.transitions.iter() {
            transitions.push(LightTransition {
                name: transition.borrow().name.clone(),
            });
        }
        return JsValue::from_serde(&transitions).unwrap();
    }

    #[wasm_bindgen(method, getter)]
    pub fn edges(&self) -> JsValue {
        let mut edges: Vec<LightEdge> = Vec::new();
        for edge in self.petrinet.edges.iter() {
            let bool_orientation: bool = match edge.borrow().orientation {
                Orientation::Transition => true,
                Orientation::Place => false,
            };
            edges.push(LightEdge {
                place: LightPlace {
                    name: edge.borrow().place.borrow().name.clone(),
                    tokens: edge.borrow().place.borrow().tokens
                },
                transition: LightTransition {
                    name: edge.borrow().transition.borrow().name.clone()
                },
                orientation: bool_orientation,
                weight: edge.borrow().weight,
            });
        }
        return JsValue::from_serde(&edges).unwrap();
    }

    pub fn display(&self) -> String {
        return self.petrinet.display();
    }
}

#[derive(Serialize, Deserialize)]
pub struct LightPlace {
    pub name: String,
    pub tokens: i32,
}

#[derive(Serialize, Deserialize)]
pub struct LightTransition {
    pub name: String,
}

#[derive(Serialize, Deserialize)]
pub struct LightEdge {
    pub place: LightPlace,
    pub transition: LightTransition,
    pub orientation: bool,
    pub weight: i32,
}
